import mongoose from 'mongoose'

const con = (mongoose) => {
    try {
        mongoose.Promise = global.Promise
        mongoose.connect('mongodb://localhost/api-vuex')
    } catch (error) {
        setTimeout(() => {
            return con(mongoose)
        }, 3000);
    }
}

con(mongoose)

module.exports = mongoose