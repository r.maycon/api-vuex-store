import multer from 'multer'
import 'babel-polyfill'
// console.log("index.js");
import app from './app/app'

const get_port = () => process.env.PORT || 3000
app.set('port', get_port())
app.listen(app.get('port'), (error) => {
    console.clear()
    console.log("Running in PORT " + app.get('port'));

})