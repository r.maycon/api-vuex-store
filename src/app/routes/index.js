export default (app) => {
    app.route('/*')
        .all((req, res, next) => {
            return res.status(404).json({
                code: "RESOURCE_NOT_FOUND",
                message: `Rota ${req.method} para ${req.path} não encontrada, verifique toda a URI e tente novamente.`,
                name: `${req.method} para ${req.path}`,
                statusCode: 404
            })
        })
}