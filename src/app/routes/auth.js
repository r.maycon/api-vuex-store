import UserControl from '../controllers/UserController'
import utils from '../../utils';
export default (app) => {
    const userControl = new UserControl(app.database.models.user)
    let list = ['name', 'password']
    app.route('/authenticate')
        .patch(async (req, res, next) => {
            const {
                email,
                password
            } = req.body
            const data = await userControl.getAll(),
                user = data.data.find(doc => {
                    return doc.email.toString() === email.toString()
                })
            if (user) {
                if (await userControl.compare(password, user.password)) {
                    let data = utils.formData(user, ['id', 'email', 'lastname', 'name'])
                    let token = utils.gentoken({
                        id: data.id
                    })
                    data.token = token.sign
                    res.status(202).json(data)
                } else {
                    return res.status(409).json({
                        code: "AUTHENTICATE_FAIL",
                        message: "Senhas não conferem, por favor verifique e tente novamente.",
                        name: "Autenticar",
                        statusCode: 409
                    })
                }
            }
            // res.end()
            // const data = await userControl.create()
        })
    app.route('/register')
        .post(async (req, res, next) => {
            let data = await userControl.create(req.body)
            if (data.statusCode === 200) {
                data.data = utils.formData(data.data, ['id', 'email', 'lastname', 'name'])
                let token = utils.gentoken({
                    id: data.data.id
                })
                data.data.token = token.sign
            }
            res.status(data.statusCode).json(data.data)
        })
}