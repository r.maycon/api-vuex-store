import mongoose from '../../database'
import bcrypt from 'bcryptjs'

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        require: true
    },
    lastname: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    password: {
        type: String,
        require: true
    }
})

UserSchema.pre('findOneAndUpdate', async function (next) {
    let data = await User.findOne({
        _id: this.getUpdate().user_id
    }).select('password')
    if (this.getUpdate().password != data.password) {
        this.update({}, {
            last_update: Date().toString(),
            password: await bcrypt.hash(this.getUpdate().password, 10),
        })
    }
    next()
})

UserSchema.pre('save', async function (next) {
    const hash = await bcrypt.hash(this.password, 10)
    this.password = hash
    next()
})

const User = mongoose.model("User", UserSchema)

export default User
// module.exports = User