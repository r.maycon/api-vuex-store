import utils from '../../utils/index'
export default (app) => {
    const help = async (req, res, next) => {
        let lista = ['name', 'lastname', 'email', 'password']
        if (utils.checkFields(req.body, lista)) {
            return next()
        } else {
            return res.status(428).json({
                code: "FIELDS_UNDEFINED",
                message: `Estes campos [ ${lista} ] não podem ficar em branco ou vazios, verifique ou preencha e tente novamente.`,
                name: "Cadatrar ou alterar dados de usuário",
                statusCode: 428
            })
        }
    }
    app.route('/users')
        .post(help)
        .patch(help)
}