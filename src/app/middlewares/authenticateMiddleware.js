import utils from '../../utils'
export default (app) => {
    const lista = ['email', 'password']
    app.route("/authenticate")
        .patch(async (req, res, next) => {
            if (utils.checkFields(req.body, lista)) {
                return next()
            } else {
                return res.status(428).json({
                    code: "FIELDS_UNDEFINED",
                    message: `Estes campos [ ${lista} ] não podem ficar em branco ou vazios, verifique ou preencha e tente novamente.`,
                    name: "Logar conta.",
                    statusCode: 428
                })
            }
        })
}