const checkFields = (data, fields) => {
    let check = true
    for (let i = 0; i < fields.length; i++) {
        if (data[fields[i]] == undefined) {
            check = false
        }
    }
    return check
}

/* funçãp para mapear e formatar os campos de um objeto em com novos nomes em outro objeto. */
const formData = (data, fields) => {
    let newData = {}
    for (let i = 0; i < fields.length; i++) {
        newData[fields[i]] = data[fields[i]]
    }
    return newData
}

import jwt from 'jsonwebtoken'
import config from '../config/secret.json'
import {
    version
} from '../../package.json'
const jwtverify = async (hash) => {
    jwt.verify(hash, config.secret, (error, decoded) => {
        if (!error) {
            return {
                status: true,
                data: decoded
            }
        } else {
            return {
                status: false
            }
        }
    })
}

const gentoken = (data = {
    version: version
}) => {
    return {
        sign: jwt.sign(data, config.secret, {
            expiresIn: 64800
        })
    }
}
export default {
    checkFields,
    formData,
    jwtverify,
    gentoken
}